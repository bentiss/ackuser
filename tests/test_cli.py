# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from click.testing import CliRunner
from unittest.mock import patch, MagicMock, NonCallableMagicMock
from pathlib import Path

import ackuser

import logging
import pytest

logger = logging.getLogger("test-cli")


# Patch the cli to use example.com as default instance (which is also used
# in the example webhook payloads). This way a runway test
# doesn't risk contacting the real fdo gitlab
@pytest.fixture(autouse=True)
def replace_default_instance():
    with patch("ackuser.cli.DEFAULT_INSTANCE", "https://example.com"):
        yield


def write_xdg_token(name: str = "userbot.token"):
    token_path = Path("ackuser") / name
    token_path.parent.mkdir(parents=True)
    with open(token_path, "w") as f:
        f.write(f"xdgtokenvalue.{name}")


@patch("ackuser.cli.ack")
def test_approve(ack, monkeypatch):
    from ackuser.cli import ackuser as ackuser_cli

    expected_url = "https://example.com/freedesktop/freedesktop/-/issues/123"
    command = ["approve", expected_url]

    # Disable the XDG token lookup
    monkeypatch.setattr(Path, "home", lambda: Path("/does-not-exist"))
    monkeypatch.setenv("OUR_TOKEN", "envtokenvalue")

    builder = MagicMock()
    builder.create_from_issue_url = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)

    # Override the entry point with our mock
    ack.AckUserBuilder = builder

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ack.AckUserIssue = NonCallableMagicMock()
    ack.USER_APPROVED_LABEL = ackuser.USER_APPROVED_LABEL

    # No token, so it fails
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(ackuser_cli, command)
        logger.debug(result.output)
        assert result.exit_code == 1
        assert "One of --token-file or --token-env is required" in result.output

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    # Token file should work
    with runner.isolated_filesystem():
        # xdg token exists but is ignored
        write_xdg_token()

        # token we want to use
        with open("token.txt", "w") as f:
            f.write("tokenvalue")

        result = runner.invoke(ackuser_cli, ["--token-file", "token.txt"] + command)
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        assert result.exit_code == 0

        builder.create_from_issue_url.assert_called_with(
            expected_url,
            "tokenvalue",
        )

    # Token env should work
    with runner.isolated_filesystem():
        # xdg token exists but is ignored
        write_xdg_token()

        result = runner.invoke(ackuser_cli, ["--token-env", "OUR_TOKEN"] + command)
        logger.debug(result.output)
        assert result.exit_code == 0

        ack.AckUserBuilder.create_from_issue_url.assert_called_with(
            expected_url,
            "envtokenvalue",
        )

    # XDG token should work
    with runner.isolated_filesystem():
        monkeypatch.setenv("XDG_CONFIG_HOME", ".")
        write_xdg_token()

        result = runner.invoke(ackuser_cli, command)
        logger.debug(result.output)
        assert result.exit_code == 0

        ack.AckUserBuilder.create_from_issue_url.assert_called_with(
            expected_url,
            "xdgtokenvalue.userbot.token",
        )
