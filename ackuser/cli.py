# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional
from pathlib import Path

import attr
import click
import logging
import os

import ackuser as ack

logger = logging.getLogger("ackuser")

DEFAULT_INSTANCE = "https://gitlab.freedesktop.org"


def xdg_config_home() -> Path:
    return Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config"))


def xdg_token(name: str = "userbot.token") -> Optional[str]:
    xdg_path = xdg_config_home() / "ackuser" / name
    try:
        token = open(xdg_path).read().rstrip()
    except FileNotFoundError:
        return None
    return token


def die(message: str):
    click.echo(message)
    raise SystemExit(1)


@attr.s
class Config:
    verbose: bool = attr.ib()
    readonly: bool = attr.ib()
    token: Optional[str] = attr.ib()


@click.group()
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.option("--readonly", is_flag=True, help="Print, but don't modify")
@click.option(
    "--token-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the GitLab API token. Defaults to XDG_CONFIG_HOME/ackuser/userbot.token",
)
@click.option(
    "--token-env",
    type=str,
    help="Environment variable containing the GitLab API token",
)
@click.pass_context
def ackuser(
    ctx,
    verbose: bool,
    readonly: bool,
    token_file: Optional[click.Path],
    token_env: Optional[str],
):
    global logger
    """
    Approve a user, i.e. untick the "external" checkbox.
    """
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if token_file is not None:
        token = open(token_file).read().rstrip() or die("Empty token file")  # type: ignore
    elif token_env is not None:
        token = os.environ.get(token_env) or die(
            "Requested token environment variable does not exist, use --token-env"
        )
    else:
        token = None

    ctx.obj = Config(verbose=verbose, readonly=readonly, token=token)


@ackuser.command(
    name="approve", help="Process the given GitLab account verification issue"
)
@click.argument(
    "issue",
    type=str,
)
@click.pass_context
def do_approve(
    ctx,
    issue: str,
):
    """
    Approve the user that filed the issue, applying the Account verification::aproved label in the process.

    The issue is 123 or https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/123
    The host name must match the --instance argument.
    """
    config = ctx.obj

    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    builder = ack.AckUserBuilder.create_from_issue_url(issue, token).set_readonly(
        config.readonly
    )

    ack_user = builder.build()
    success = ack_user.approve()
    if success:
        message = "All done. Welcome and thanks for your future contributions! :tada:"
    else:
        message = "I was asked to look at this issue, but something went wrong. I'm just a simple bot. Please approve manually."

    ack_user.update_with_message(message, close=success, is_success=success)


if __name__ == "__main__":
    ackuser()
