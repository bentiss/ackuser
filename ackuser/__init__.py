# SPDX-Licences-Identifier: MIT

__version__ = "0.1.0"

from typing import Any, Optional, Union

import attr
import gitlab
import gitlab.v4.objects
import logging
import re

logging.basicConfig(format="%(levelname)7s| %(name)s: %(message)s")
logger = logging.getLogger("ackuser")

USERBOT_USER_ID = 87895  # @userbot
FDO_PROJECT_ID = 1  # freedesktop/freedesktop

ACCOUNT_VERIFICATION_LABEL = "Account verification"
USER_APPROVED_LABEL = "Account verification::approved"


def not_read_only(ro: Union[Any, bool], message) -> bool:
    """
    Helper function for a read-only context that only executes the condition
    if readonly is `False`. Usage:

    >>> if not_read_only(self.readonly, "Deleting user"):
            user.delete()

    The user is only deleted if `self.readonly` is False.
    """
    if isinstance(ro, bool):
        readonly = ro
    else:
        assert hasattr(ro, "readonly")
        readonly = ro.readonly
    prefix = "[RO SKIP] " if readonly else ""
    logger.info(f"{prefix}{message}")
    return not readonly


@attr.s
class BuilderError(Exception):
    message: str = attr.ib()

    def __str__(self):
        return f"Builder Error: {self.message}"


@attr.s
class AckUserBuilder:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    issue: gitlab.v4.objects.ProjectIssue = attr.ib()
    user: Optional[gitlab.v4.objects.User] = attr.ib(init=False, default=None)
    # We default to readonly true so a runaway bot can't change anything and any copies of
    # this object explicitly needs to disable readonly
    readonly: bool = attr.ib(init=False, default=True)

    @classmethod
    def create_from_issue_url(
        cls, url: str, private_token: str, gl: Optional[gitlab.Gitlab] = None
    ) -> "AckUserBuilder":
        logger.debug(f"Using GitLab issue: {url}")
        match = re.match(
            r"(?P<instance>http[s]?://[^/]+)/(?P<project>[\w_/-]+)/-/issues/(?P<issue>[0-9]+)",
            url,
        )
        if not match:
            raise BuilderError(f"Invalid URL {url}")

        try:
            instance, path_with_namespace, issue_iid = match["instance"], match["project"], int(match["issue"])  # type: ignore
        except (TypeError, IndexError, ValueError):
            raise BuilderError(f"Invalid URL {url}")

        gl = gl or gitlab.Gitlab(url=instance, private_token=private_token)  # type: ignore
        gl.auth()

        logger.debug(f"Using GitLab project: {path_with_namespace}")
        project = gl.projects.get(id=path_with_namespace)
        if project is None:
            raise BuilderError(f"Project {path_with_namespace} not found")

        issue = project.issues.get(id=issue_iid)
        if issue is None:
            raise BuilderError(f"Project issue {issue_iid} not found")

        return cls(gl=gl, project=project, issue=issue)

    def set_readonly(self, readonly: bool) -> "AckUserBuilder":
        self.readonly = readonly
        return self

    def build(self) -> "AckUserIssue":
        if any([x is None for x in (self.project, self.issue, self.gl)]):
            raise BuilderError("Cannot build without all fields set")

        # to shut up mypy:
        assert self.gl is not None
        assert self.project is not None
        assert self.issue is not None
        return AckUserIssue(
            gl=self.gl,
            project=self.project,
            issue=self.issue,
            readonly=self.readonly,
        )


@attr.s
class AckUserIssue:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    issue: gitlab.v4.objects.ProjectIssue = attr.ib()
    readonly: bool = attr.ib(default=False)

    def approve(self) -> bool:
        author = self.gl.users.get(id=self.issue.author["id"])
        if not author.external:
            logger.debug(f"User {author.name} is not external, we're done")
            return True

        if not_read_only(
            self, f"Setting user {author.name} (@{author.username}) to internal"
        ):
            author.external = False
            author.save()

        return True

    def update_with_message(
        self, message: str, close: bool, is_success: Optional[bool] = None
    ):
        """
        Update the issue with the message and optionally closing it.
        This also assigns the ackuser label and if is_succcess is True
        or False (but not None), the ackuser::success/failed label.
        """
        if not_read_only(self, message):
            self.issue.assignee_ids = [USERBOT_USER_ID]
            labels = self.issue.labels + [ACCOUNT_VERIFICATION_LABEL]
            labels += {
                True: ["ackuser::success"],
                False: ["ackuser::failed"],
                None: [],
            }[is_success]
            self.issue.labels = labels
            self.issue.notes.create({"body": message})
            if close:
                self.issue.state_event = "close"
            self.issue.save()
